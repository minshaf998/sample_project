FROM node:18.12.0-alpine3.16
WORKDIR /app
COPY package*.json .
RUN npm install
RUN apk update
RUN apk add git
RUN apk add bash
COPY . .
EXPOSE 3000
CMD ["npm", "start"]